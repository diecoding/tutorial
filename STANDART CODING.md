# Dillinger

### Function & Variable

Cara penulisan function & variable:
1. Nama `function` dan `variable` harus diawali dengan huruf kecil dan disambung dengan huruf kapital di setiap spasinya ex. `funName` bukan `FunName`.
2. Ketika akan menampilkan `return`, wajib memberi `1 spasi sebelum` kata return.
3. Harus menuliskan doc. sebelum function ()
```php
/**
 * Description
 *
 * @params string $params
 * @return string
 */
```
4. Karakter `{` harus berada `tepat 1 baris di bawah function()` bukan 1 baris dengan function
5. Menggunakan Tab `4 Spasi`
6. Beautify sangat diutamakan.
7. Hindari pengulangan deklarasi

Standart:
```php
/**
 * Description
 *
 * @params string $params
 * @return string
 */
public function funName($params)
{
    $model = $params;

    // enter 1 baris untuk return
    return $model;
}
```

Non Standart:
```php
public function funName($params){
    $model = $params;
    return $model;
}
```

```php
public function FunName($Params)
{
    $Model = $Params;
    return $Model;
}
```
